# Excel Accounts Backend

[Excel Accounts Backend](https://gitlab.com/excel-mec/excel-accounts/Excel-Accounts-Backend) manages the back-end functionalities of the  Excel Accounts Service. It's written in [ASP.Net Core 3.1](https://docs.microsoft.com/en-us/aspnet/core/?view=aspnetcore-3.1). The project follows Repository pattern. 



## Directory Structure

### [API - Main project directory](/Accounts/Backend/API/API.md) 

- `Assets`  - Projects assets folder.
- `Controllers` -  The API endpoints.
- `Data` - Repository classes/ services.
- `Services` - Custom services
- `Extensions` -  Middlewares for custom services and repository services.
- `Dtos`-  Used to map Data to Object .
- `Helpers` -  Helper classes for  Initial seeding of  data, Automapper classes and [Swagger](https://swagger.io/) Configuration file.
- `Models`- Database schema
- `appsettings` - Constants used for configuration.

### [Tests - Project for unit test](/Accounts/Backend/API/API.md) 

- `ControllerTests` 
- `ServiceTests`

### [Excel-Accounts-Backend - The solution file.]()



