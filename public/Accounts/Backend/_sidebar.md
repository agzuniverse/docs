* [Welcome To Excel Platform](/) 

  * [Excel Accounts](/Accounts/Accounts-Intro.md) 
    * [Excel-Accounts-Auth](/Accounts/Auth/Accounts-Auth.md)
    * [Excel-Accounts-Backend](/Accounts/Backend/Accounts-Backend.md)
      * [API](/Accounts/Backend/API/API.md)
      * [Tests](/Accounts/Backend/Tests/Tests.md)
    * [Excel-Accounts-Frontend](/Accounts/Frontend/Accounts-Frontend.md)

    

    

  

  