*  * * [Welcome To Excel Platform](/) 
    
      * [Excel Accounts](/Accounts/Accounts-Intro.md) 
        * [Excel-Accounts-Auth](/Accounts/Auth/Accounts-Auth.md)
        * [Excel-Accounts-Backend](/Accounts/Backend/Accounts-Backend.md)
          * [API](/Accounts/Backend/API/API.md)
            * [Controllers](/Accounts/Backend/API/Controllers/Controllers.md)
            * [Extensions](/Accounts/Backend/API/Extensions/Extensions.md)
            * [Data](/Accounts/Backend/API/Data/Data.md)
            * [Services](/Accounts/Backend/API/Services/Services.md)
            * [Models](/Accounts/Backend/API/Models/Models.md)
          * [Tests](/Accounts/Backend/Tests/Tests.md)
        * [Excel-Accounts-Frontend](/Accounts/Frontend/Accounts-Frontend.md)
    
      