# Models

Models contain the database schemas.

## User

Data model for Users.

```c#
public class User
{
    public int Id { get; set; }
    [Required]
    public string Name { get; set; }
    [Required]
    public string Email { get; set; }
    public string Picture { get; set; }
    public string QRCodeUrl { get; set; }
    public int InstitutionId { get; set; }
    public string Gender { get; set; }
    public string MobileNumber { get; set; }
    public string Category{ get; set; }
}
```



## College

Data model for Colleges.

```c#
public class College
{
    public int Id { get; set; }
    public string Name { get; set; }
}
```



## School

Data model for Schools.

```c#
public class School
{
    public int Id { get; set; }
    public string Name { get; set; } 
}
```



Check out more about Models from [here](https://gitlab.com/excel-mec/excel-accounts/Excel-Accounts-Backend/-/tree/master/API/Models).