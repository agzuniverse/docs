# Extensions

> A Startup Class extension.

In order to reduce the length of the Startup class, Custom Services, Repository Services and Custom Exception Handler is kept in the [Extensions](https://gitlab.com/excel-mec/excel-accounts/Excel-Accounts-Backend/-/tree/master/API/Extensions).

## ExceptionMiddlewares

[ExceptionMiddlewares](https://gitlab.com/excel-mec/excel-accounts/Excel-Accounts-Backend/-/blob/master/API/Extensions/ExceptionMiddlewares.cs) contain the ConfigureExceptionHandlerMiddleware method which handles  the exceptions.  `UnauthorizedAccessException` in the middleware pipeline is handled by returning a 401 Status Code as the response.

## Repository Services

AddRepositoryServices method adds the repository classes to the service container. Given is a list of repository classes:

- Auth Respository
- Profile Repository
- Institution Repository

Check out more about repository services from [here](/Accounts/Backend/API/Data/Data?id=repository-services).

## Custom Services

AddRepositoryServices method adds the custom services to the service container. Given is a list of such services:

- Auth Service
- Profile Service
- GoogleCloudStorage 
- QRCode Generation 
- Cipher Service

Check out more about custom services from [here](/Accounts/Backend/API/Services/Services?id=custom-services).
