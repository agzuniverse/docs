## Repository Services

### Auth Respository

A repository which is concerned about the authentication.

#### Register Method 

Method to register a new user.

```c#
Task<User> Register(User user);
```

#### UserExists Method

Method to check whether the user with the given Email ID is  already registered.

```c#
Task<bool> UserExists(string email);
```

#### GetUser Method

Method to retrieve a user with the specified Email ID.

```c#
Task<User> GetUser(string email);
```



### Profile Repository

A repository which is concerned about the profile details of a registerd user.

#### GetUser

Method to retrieve a user with the specified Excel ID.

```c#
Task<User> GetUser(int id);
```

#### UpdateProfile

Method to update the profile of a user( *except profile image* ) with the given Excel ID.

```c#
Task<bool> UpdateProfile(int id, UserForProfileUpdateDto data);
```

#### UpdateProfileImage

Method to update the profile image of a user with the given Excel ID.

```c#
Task<bool> UpdateProfileImage(int id, string imageUrl);
```



### Institution Repository

A repository which is concerned about the institution details.

#### AddCollege

Method to add a new college to the existing college list.

```c#
Task<College> AddCollege(string Name);
```

#### AddSchool

Method to add a new school to the existing school list.

```c#
Task<School> AddSchool(string Name);
```

#### CollegeList

Method to retrieve a list of colleges.

```c#
Task<List<College>> CollegeList();
```

#### SchoolList

Method to retrieve a list of schools.

```c#
Task<List<School>> SchoolList();
```

#### FindName

Method to find the name of the institution with the given ID.

```c#
Task<string> FindName(string category, int id);
```



## DataContext

The [DataContext](https://gitlab.com/excel-mec/excel-accounts/Excel-Accounts-Backend/-/blob/master/API/Data/DataContext.cs) is the source of all entities mapped over a database connection. It tracks changes that you made to all retrieved entities and maintains an `identity cache` that guarantees that entities retrieved more than one time are represented by using the same object instance.



Check out more  from [here](https://gitlab.com/excel-mec/excel-accounts/Excel-Accounts-Backend/-/tree/master/API/Data).