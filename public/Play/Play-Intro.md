# Excel Play

[Excel Play](https://github.com/Excel-MEC/excelplay-backend-service) is the online events platform of Excel, the annual techno-managerial fest of Govt. [Model Engineering College](http://mec.ac.in/).

It has a common frontend for all the events and separate backends for each event. Currently these events are Kryptos and Dalalbull.

- [Excel Play Frontend](./Frontend/Readme.md) ([Repo](https://gitlab.com/excel-mec/excelplay/excelplay-frontend))
- [Kryptos Backend](./Kryptos/Readme.md) ([Repo](https://github.com/Excel-MEC/excelplay-backend-kryptos))
- [Dalalbull Backend](./Dalalbull/Readme.md) ([Repo](https://github.com/Excel-MEC/excelplay-backend-dalalbull))

## Setting up Development Environment

### Requirements:

- You must have [docker](https://docker.com/) and docker-compose installed to run the application.
- Installing [go](https://golang.org/) is recommended, although you can run everything inside the docker containers without it.
- Installing `go` and [swag](https://github.com/swaggo/swag) is necessary for generating documentation.

### Clone the repo:

Fork the repository to your account and clone it to your device:

```sh
$ git clone --recurse-submodules https://github.com/Excel-MEC/excelplay-backend-service
```

### Running the Application:

Use docker-compose for running all components required for the project:

```sh
$ docker-compose up --build
```

Once built, You can run the containers without rebuilding by simply running:

```sh
$ docker-compose up
```

To bring down all the containers and volumes that the command started, use:

```sh
$ docker-compose down --remove-orphans --volumes
```

All environment variables needed for docker can be found in .env file in the project root.
PGAdmin can be used to manipulate the database manually and is available on `localhost:5050` after running docker-compose. The login details can be found in the .env file and the IP of the postgres container that PGAdmin needs can be found by running `docker inspect <postgres container name> | grep "IP"`.

To use endpoints that require auth access, [create a JWT](http://jwtbuilder.jamiekurtz.com/) and pass it in the Authorization header in the format `Bearer <token>`. The `user_id` field of the JWT must contain a valid integer user id. The JWT must also include a `name` field with the user's name.

## Generating docs

Swagger UI compatible docs are generated using [swaggo](https://github.com/swaggo/swag).

Download and install the `swag` binary from that repository (typically by moving the binary to `/usr/local/go/bin/`), then from the root folder of either kryptos or dalalbull, run

```shell
$ swag init --generalInfo ../../cmd/excelplay-backend-kryptos/main.go --dir ./pkg/handlers --output ./cmd/excelplay-backend-kryptos/docs
```

This very specific command is required, because for some reason the file where swaggo looks for general info (which in this case, is the `main.go` file) is relative to the directory specified with the `--dir` option, but the output folder specified with the `--output` option is relative to the directory from which this command is executed. If the output docs folder is not in the same directory as `main.go`, further problems arise. The directory specified with the `--dir` option is the one containing the handlers for all the API endpoints.

## Note on go modules

This project uses go modules but includes vendored deps to support older go versions. A go version that supports go modules is required to add new dependencies. Once a new dependency is added use `go mod vendor` to vendor the new dependency for people on older go versions.

## Troubleshooting:

Make sure no processes are running in the following ports in localhost:

- :8080
- :5050
- :5432

## Contributing

### Issues

If you notice a bug or an issue, do not hesitate to open an issue in the appropriate repo to bring it to the notice of the devs or to fix it yourself later on.

Make sure that issues related to Kryptos and Dalalbull are filed in their respective repos instead of the `excelplay-backend-service` repo.

### Pull Requests

Please follow these guidelines when submitting a PR:

- If possible, open an issue first describing in detail the bug or enhancement, and mention that you are working on it.
- Have clear commit messages that describe exactly what the change is.
- Each commit should be self contained - two or more unrelated changes should not be in the same commit.
- Describe the change you are making, and why you are making it in the PR body.
- Make sure the PR passes CI.
